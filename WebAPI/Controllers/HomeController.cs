﻿namespace WebAPI.Controllers
{
    using System.Web.Http;
    using Common;

    public class HomeController : ApiController
    {
        private IMessenger _messager;

        public HomeController(IMessenger messenger)
        {
            _messager = messenger;
        }

        [HttpGet]
        public string SayHello()
        {
            return _messager.Message;
        }
    }
}
