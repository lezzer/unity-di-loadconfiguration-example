﻿namespace Common
{
    public class TestMessenger: IMessenger
    {
        public string Message 
        { 
            get { return "Hello from the TestMessenger!!"; } 
        }
    }
}
