﻿namespace Common
{
    public class LiveMessenger : IMessenger
    {
        public string Message
        {
            get { return "Hello from LiveMessenger!"; }
        }
    }
}
