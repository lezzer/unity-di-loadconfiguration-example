﻿namespace Common
{
    public interface IMessenger
    {
        string Message { get; }
    }
}
