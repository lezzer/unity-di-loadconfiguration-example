﻿namespace OwinSelfHosting
{
    using System;
    using App_Start;
    using Microsoft.Owin.Hosting;
    using WebAPI.Controllers;

    class Program
    {
        static void Main(string[] args)
        {
            const string baseAddress = "http://localhost:12345/";
            Type ApplicationController = typeof(HomeController);
            
            using (WebApp.Start<WebApiStartUp>(url: baseAddress))
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("I don't wish to alarm you but WebAPI is listening to everything you say...");
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(baseAddress);
                Console.ReadLine();
            }
        }
    }
}
