﻿namespace OwinSelfHosting.App_Start
{
    using System.Web.Http;
    using Microsoft.Practices.Unity;
    using Microsoft.Practices.Unity.Configuration;
    using Owin;
    using Unity;

    public class WebApiStartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );



            // Approach 1:  My preferred approach.  Have multiple named containers in the app.config.
            //              One for LIVE, one for TEST for example.  Make sure you add name="" to the <container> element.
            //              DO NOT add name="" to any of the <register type="" elements.  These will be auto resolved.
            //              Call LoadConfiguration("{name}") passing in the name of the <container> element you wish to use.
            var container = new UnityContainer();
            container.LoadConfiguration("liveContainer");


            // Approach 2:  Less favourable in this case.  Have a single <container> element that contains multiple <register type=""
            //              elements.  For example:
            //              <register type="IMessenger" mapTo="LiveMessenger" name="liveMessenger" />
            //              <register type="IMessenger" mapTo="TestMessenger" name="testMessenger" />
            //              Because there are now multiple concrete types for each contract, you must explicitly tell it how to 
            //              resolve the types.
            //              Pass in the name of the <register type== element that you wish to resolve see the additional line below
            //var container = new UnityContainer();
            //container.LoadConfiguration();
            //var messenger = container.Resolve<IMessenger>("liveMessenger");            
            
            
            config.DependencyResolver = new UnityResolver(container);
            config.MapHttpAttributeRoutes();
            appBuilder.UseWebApi(config);
        }
    }
}
